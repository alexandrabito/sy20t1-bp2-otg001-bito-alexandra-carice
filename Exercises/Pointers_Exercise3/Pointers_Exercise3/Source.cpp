#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

struct Item
{
	string name; //name of item
	int goldValue; // gold value of item
	//int multiplier; // item's bonus multiplier

};

Item* generateItems (vector<Item*> inventory, int& goldLooted, int& bonusMithrilOre, int& bonusSharpTalon, int& bonusThickLeather, int& bonusJellopy, bool& insideDungeon)
{
	Item* item0 = new Item;
	item0->name = "Mithril Ore";
	item0->goldValue = 100;
	/*item0->multiplier = 1;*/

	Item* item1 = new Item;
	item1->name = "Sharp Talon";
	item1->goldValue = 50;
	/*item1->multiplier = 1;*/

	Item* item2 = new Item;
	item2->name = "Thick Leather";
	item2->goldValue = 25;
	/*item2->multiplier = 1;*/

	Item* item3 = new Item;
	item3->name = "Jellopy";
	item3->goldValue = 5;
	/*item3->multiplier = 1;*/

	Item* item4 = new Item;
	item4->name = "Cursed Stone";
	item4->goldValue = 0;
	/*item4->multiplier = 1;*/

	inventory.push_back(item0);
	inventory.push_back(item1);
	inventory.push_back(item2);
	inventory.push_back(item3);
	inventory.push_back(item4);

	int itemPicked = rand() % 5;

	if (itemPicked == 0)
	{
		cout << item0->name << " = " << item0-> goldValue << " gold" << endl;
		cout << "Bonus multplier: x" << bonusMithrilOre << endl;

		item0->goldValue *= bonusMithrilOre;
		goldLooted += item0->goldValue;
		bonusMithrilOre += 1; // bonus multiplier

		return item0;		
	}
	else if (itemPicked == 1)
	{
		cout << item1->name << " = " << item1->goldValue << " gold" << endl;		
		cout << "Bonus multplier: x" << bonusSharpTalon << endl;

		item1->goldValue *= bonusSharpTalon;
		goldLooted += item1->goldValue;
		bonusSharpTalon += 1;

		return item1;		
	}
	else if (itemPicked == 2)
	{
		cout << item2->name << " = " << item2->goldValue << " gold" << endl;
		cout << "Bonus multplier: x" << bonusThickLeather << endl;

		item2->goldValue *= bonusThickLeather;
		goldLooted += item2->goldValue;
		bonusThickLeather += 1;

		return item2;
	}
	else if (itemPicked == 3)
	{
		cout << item3->name << " = " << item3->goldValue << " gold" << endl;
		cout << "Bonus multplier: x" << bonusJellopy << endl;

		item3->goldValue *= bonusJellopy;
		goldLooted += item3->goldValue;
		bonusJellopy += 1;

		return item3;
	}
	else if (itemPicked == 4)
	{
		cout << item4->name << " = " << item4->goldValue << " gold" << endl;
		cout << "Oh no! The player has lost " << goldLooted << " gold they have looted." << endl;
		goldLooted = 0;

		insideDungeon = false;
		return item4;
	}
}

void gameCondition(int& gold, int& goldLooted, bool& insideDungeon)
{
	if (goldLooted < 500)
	{
		cout << "Player hasn't reached their total target." << endl;
		cout << " " << endl;
	}
	else if (goldLooted >= 500)
	{
		cout << "Player has reached their target." << endl;
		cout << "Player wins." << endl;
		cout << " " << endl;
		insideDungeon = false;
	}
	else if (gold && goldLooted < 25)
	{
		cout << "Player does not have enough money to enter the dungeon." << endl;
		cout << " " << endl;
		insideDungeon = false;
	}

}

void enterDungeon(vector<Item*> inventory, int& gold, int& goldLooted, string playerAnswer, int& bonusMithrilOre, int& bonusSharpTalon, int& bonusThickLeather, int& bonusJellopy, bool& insideDungeon)
{
	gameCondition(gold, goldLooted, insideDungeon);
	generateItems(inventory, goldLooted, bonusMithrilOre, bonusSharpTalon, bonusThickLeather, bonusJellopy, insideDungeon);

	cout << "Player's gold: " << gold << endl;
	cout << "Gold looted: " << goldLooted << endl;
	cout << " " << endl;
	cout << "Continue looting? (yes/no)" << endl;
	cout << "----------------------------" << endl;
	cin >> playerAnswer;
	cout << " " << endl;

	if (playerAnswer == "yes" || goldLooted == 0)
	{
		cout << "Player continues to explore the dungeon..." << endl;
		cout << " " << endl;

		if (goldLooted == 0) // when they loot a cursed stone
		{
			cout << "The player has died and has lost all their gold." << endl;
			system("pause");
			system("cls");
			insideDungeon = false;
		}

	}
	else if (playerAnswer == "no" || goldLooted == 0)
	{
		gameCondition(gold, goldLooted, insideDungeon);
		cout << "Player has exit the dungeon" << endl; // choose no to exit
		gold += goldLooted;
		cout << "Gold earned: " << gold << endl;

		system("pause");
		system("cls");

		if (goldLooted == 0) // when they loot a cursed stone
		{
			cout << "The player has died and has lost all their gold." << endl;
			system("pause");
			system("cls");
			insideDungeon = false;
		}
		
	}
	
}

int main()
{
	srand(time(NULL));

	//Variables
	vector<Item*> inventory;
	int gold = 50,
		goldLooted = 0,
		bonusMithrilOre = 1,
		bonusSharpTalon = 1,
		bonusThickLeather = 1,
		bonusJellopy = 1;
	string answer, playerAnswer;
	bool insideDungeon = true;

	//Start
	cout << "Player's gold: " << gold << endl; //Display player's gold
	cout << "Entering Dungeon..." << endl;
	cout << "----------------------------" << endl;
	gold -= 25; // Entering dungeon costs 25 gold

	system("pause");
	system("cls");

	while (insideDungeon == true) //While player hasn't looted Cursed Stone
	{
		enterDungeon(inventory, gold, goldLooted, playerAnswer, bonusMithrilOre, bonusSharpTalon, bonusThickLeather, bonusJellopy, insideDungeon);
	}
}