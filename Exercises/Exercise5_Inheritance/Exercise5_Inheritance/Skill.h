#pragma once
#include <string>

using namespace std;

class Unit;

class Skill
{
public:
	Skill(string name, Unit* actor);

	string getName();
	Unit* getActor();
	
	virtual void activate(Unit* actor) = 0;

private:
	string mName;
	Unit* mActor;
	  
	 
};

