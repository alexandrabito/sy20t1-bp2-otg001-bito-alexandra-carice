#include "Heal.h"
#include "Unit.h"
#include <iostream>
#include <string>

using namespace std;

Heal::Heal(string name, Unit* actor)
	: Skill("Heal", actor)
{
}

void Heal::activate(Unit* actor)
{
	cout << getActor()->getName() << " used Heal!" << endl;
	actor->setHp(actor->getHp() + 10);


}
