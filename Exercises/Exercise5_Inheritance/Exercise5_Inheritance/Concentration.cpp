#include "Concentration.h"
#include "Unit.h"
#include <iostream>
#include <string>

using namespace std;

Concentration::Concentration(string name, Unit* actor)
	:Skill("Concentration", actor)
{
}

void Concentration::activate(Unit* actor)
{
	cout << actor->getName() << " used Concentration!" << endl;
	actor->setDexterity(actor->getDexterity() + 2);
}
