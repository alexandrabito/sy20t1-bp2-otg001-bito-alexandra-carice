#pragma once
#include <string>
#include <vector>

using namespace std;

class Skill;

class Unit
{
public:
	Unit(string name, int hp, int power, int vitality, int dexterity, int agility);

	//Getters
	string getName();
	int getHp();
	int getPower();
	int getVitality();
	int getDexterity();
	int getAgility();

	//Setters
	void setHp(int value);
	void setPower(int value);
	void setVitality(int value);
	void setDexterity(int value);
	void setAgility(int value);

	//Behaviors
	void move();

	//Active Skills
	Skill* Heal();
	Skill* Might();
	Skill* IronSkin();
	Skill* Concentration();
	Skill* Haste();



private:
	string mName;
	int mHp;
	int mPower;
	int mVitality;
	int mDexterity;
	int mAgility;

	vector<Skill*> mSkills;

};

