#include "IronSkin.h"
#include "Unit.h"
#include <iostream>
#include <string>

using namespace std;

IronSkin::IronSkin(string name, Unit* actor)
	:Skill("Iron Skin", actor)
{
}

void IronSkin::activate(Unit* actor)
{
	cout << getActor()->getName() << " used Iron Skin!" << endl;
	actor->setVitality(actor->getVitality() + 2);

}
