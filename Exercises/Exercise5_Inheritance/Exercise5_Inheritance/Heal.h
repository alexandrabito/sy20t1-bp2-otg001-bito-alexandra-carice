#pragma once
#include "Unit.h"
#include "Skill.h"

class Heal:
	public Skill
{
public:
	Heal(string name, Unit* actor);

	void activate(Unit* actor) override;

private:
	
	string mName;
	
};

