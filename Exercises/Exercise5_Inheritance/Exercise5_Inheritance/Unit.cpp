#include <iostream>
#include <string>
#include "Unit.h"
#include "Skill.h"

Unit::Unit(string name, int hp, int power, int vitality, int dexterity, int agility)
{
    mName = name;
    mHp = hp;
    mPower = power;
    mVitality = vitality;
    mDexterity = dexterity;
    mAgility = agility;
}

string Unit::getName()
{
    return mName;
}

int Unit::getHp()
{
    return mHp;
}

int Unit::getPower()
{
    return mPower;
}

int Unit::getVitality()
{
    return mVitality;
}

int Unit::getDexterity()
{
    return mDexterity;
}

int Unit::getAgility()
{
    return mAgility;
}

void Unit::setHp(int value)
{
    mHp = value;
}

void Unit::setPower(int value)
{
    mPower = value;
}

void Unit::setVitality(int value)
{
    mVitality = value;
}

void Unit::setDexterity(int value)
{
    mDexterity = value;
}

void Unit::setAgility(int value)
{
    mAgility = value;
}

void Unit::move()
{
    cout << mName << " is moving ..." << endl;
}

Skill* Unit::Heal()
{
    return nullptr;
}

Skill* Unit::Might()
{
    return nullptr;
}

Skill* Unit::IronSkin()
{
    return nullptr;
}

Skill* Unit::Concentration()
{
    return nullptr;
}

Skill* Unit::Haste()
{
    return nullptr;
}






