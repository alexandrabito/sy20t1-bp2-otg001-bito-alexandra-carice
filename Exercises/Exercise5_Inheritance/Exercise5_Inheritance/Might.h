#pragma once
#include "Unit.h"
#include "Skill.h"

class Might:
	public Skill
{
public: 
	Might(string name, Unit* actor);

	void activate(Unit* target) override;

private:
	string mName;
};

