#pragma once
#include "Unit.h"
#include "Skill.h"

class Haste:
	public Skill
{
public:
	Haste(string name, Unit* actor);

	void activate(Unit* actor) override;

private:

	string mName;
};

