#include "Might.h"
#include "Unit.h"
#include <iostream>
#include <string>

using namespace std;


Might::Might(string name, Unit* actor)
	:Skill ("Might", actor)
{
}

void Might::activate(Unit* actor)
{
	cout << getActor()->getName() << " used Might!" << endl;
	actor->setPower(actor->getPower() + 2);
}
