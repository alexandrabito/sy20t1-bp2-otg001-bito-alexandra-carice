#pragma once
#include "Unit.h"
#include "Skill.h"

class IronSkin:
	public Skill
{
public:
	IronSkin(string name, Unit* actor);

	void activate(Unit* target) override;

private:
	string mName;
};

