#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "Unit.h"
#include "Skill.h"

#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Concentration.h"
#include "Haste.h"



using namespace std;

int main()
{
	srand(time(NULL));


	//INITIALIZING UNIT
	Unit* unit = new Unit("Warrior", 100, 20, 18, 16 ,14);
	
	vector<Skill*> getSkill;

	Heal* heal1 = new Heal("Heal", unit);
	getSkill.push_back(heal1);

	Might* might1 = new Might("Might", unit);
	getSkill.push_back(might1);

	IronSkin* ironSkin1 = new IronSkin("Iron Skin", unit);
	getSkill.push_back(ironSkin1);

	Concentration* concentration1 = new Concentration("Concentration", unit);
	getSkill.push_back(concentration1);

	Haste* haste1 = new Haste("Haste", unit);
	getSkill.push_back(haste1);

	while (true)
	{
		//DISPLAYING OF UNIT STATS
		cout << "NAME:" << unit->getName() << endl;
		cout << "HP: " << unit->getHp() << endl;
		cout << "POWER: " << unit->getPower() << endl;
		cout << "VITALITY: " << unit->getVitality() << endl;
		cout << "DEXTERITY: " << unit->getDexterity() << endl;
		cout << "AGILITY: " << unit->getAgility() << endl;

		cout << endl;

		unit->move(); //Will move before uses skill

		cout << endl;
		system("pause");

		int randomSkill = rand() % getSkill.size();

		for (int i = 0 + 1; i < getSkill.size(); i++)
		{
			Skill* skill = getSkill[i];

			if (i == randomSkill)
			{
				skill->activate(skill->getActor());
			}
			
		}

		cout << endl;
	
	}
	  
	
}