#include "Haste.h"
#include "Unit.h"
#include <iostream>
#include <string>

using namespace std;


Haste::Haste(string name, Unit* actor)
	: Skill("Haste", actor)
{
}

void Haste::activate(Unit* actor)
{
	cout << actor->getName() << " used Haste!" << endl;
	actor->setAgility(actor->getAgility() + 2);
}
