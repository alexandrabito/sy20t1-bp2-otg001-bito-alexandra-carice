#pragma once
#include "Unit.h"
#include "Skill.h"


class Concentration:
	public Skill
{
public:
	Concentration(string name, Unit* actor);

	void activate(Unit* actor) override;

private:
	string mName;
	
};

