#include <iostream>
#include <string>
#include <time.h>
#include "Wizard.h"
#include "Spell.h"

using namespace std;

class Wizard;
class Spell;

int main()
{
	srand((NULL));

	//Instantiate
	string name1, name2, target;

	// Wizard 1
	Wizard* wizard1 = new Wizard();
	Spell* spell1 = new Spell();

	cout << "What is the name of Wizard 1? ";
	cin >> name1;

	wizard1->mName = name1;
	wizard1->mHp = 250;
	wizard1->mMp = 0;
	wizard1->mMinimumAttackDamage = 10;
	wizard1->mMaximumAttackDamage = 15;
	
	spell1->mName = name1;
	spell1->mMinimumSpellDamage = 40;
	spell1->mMaximumSpellDamage = 60;
	spell1->mMpCost = 50;

	//Wizard 2
	Wizard* wizard2 = new Wizard();
	Spell* spell2 = new Spell();

	cout << "What is the name of Wizard 2? ";
	cin >> name2;

	wizard2->mName = name2;
	wizard2->mHp = 250;
	wizard2->mMp = 0;
	wizard2->mMinimumAttackDamage = 10;
	wizard2->mMaximumAttackDamage = 15;

	spell2->mName = name2;
	spell2->mMinimumSpellDamage = 40;
	spell2->mMaximumSpellDamage = 60;
	spell2->mMpCost = 50;


	//The Game

	while (wizard1->mHp > 0 && wizard2->mHp > 0)
	{
		cout << endl;

		//Display of Wizard
		cout << "Wizard 1" << endl;
		cout << "============" << endl;
		cout << "Name: " << wizard1->mName << endl;
		cout << "HP: " << wizard1->mHp << endl;
		cout << "MP: " << wizard1->mMp << endl;
		cout << endl;

		cout << "Wizard 2" << endl;
		cout << "============" << endl;
		cout << "Name: " << wizard2->mName << endl;
		cout << "HP: " << wizard2->mHp << endl;
		cout << "MP: " << wizard2->mMp << endl;

		cout << endl;

		//Wizard 1 Turn
		if (wizard1->mMp < 50)
		{
			wizard1->attack(wizard2);
		}
		else
		{
			spell1->activate(wizard2);
			wizard1->mMp -= spell1->mMpCost;
		}
		
		cout << endl;

		//Wizard 2 Turn
		if (wizard2->mMp < 50)
		{
			wizard2->attack(wizard1);
		}
		else
		{
			spell2->activate(wizard2);
			wizard2->mMp -= spell2->mMpCost;
		}

		cout << endl;
		system("pause");
	}

	if (wizard1->mHp == 0)
	{
		delete wizard1;
		cout << wizard2->mName << " wins the battle!" << endl;
	}
	else if (wizard2->mHp == 0)
	{
		delete wizard2;
		cout << wizard1->mName << " wins the battle!" << endl;
	}
	

}