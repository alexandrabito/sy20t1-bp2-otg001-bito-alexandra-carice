#include "Spell.h"
#include "Wizard.h"

class Wizard;

void Spell::activate(Wizard* target)
{

	cout << mName << " is activating spell..." << endl;

	int finalAttack = rand() % (mMaximumSpellDamage - mMinimumSpellDamage + 1) + mMinimumSpellDamage;

	cout << mName << " casted a spell and did " << finalAttack << " damage" << endl;

	target->mHp -= finalAttack;
}

