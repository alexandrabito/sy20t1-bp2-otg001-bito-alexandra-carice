#pragma once
#include <iostream>
#include <string>
#include "Wizard.h"

using namespace std; 

class Wizard;

class Spell
{
public:
	//Properties of Spell Class
	string mName;
	int mMinimumSpellDamage;
	int mMaximumSpellDamage;
	int mMpCost;

	//Methods
	void activate(Wizard* target);

};

