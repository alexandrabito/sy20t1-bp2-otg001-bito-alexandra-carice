#pragma once
#include <iostream>
#include <string>
#include "Spell.h"

using namespace std;

class Wizard
{
public:
	//Properties of Wizard (Data Members)
	string mName;
	int mHp;
	int mMp;
	int mMinimumAttackDamage;
	int mMaximumAttackDamage;
	
	//Methods
	void attack(Wizard* target);
};

