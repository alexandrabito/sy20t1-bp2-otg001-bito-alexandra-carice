#include "Wizard.h"
#include "Spell.h"

void Wizard::attack(Wizard* target)
{
	cout << mName << " is attacking..." << endl;

	int finalAttack = rand() % (mMaximumAttackDamage - mMinimumAttackDamage + 1) + mMinimumAttackDamage; //randomizes attack damage

	cout << mName << " did " << finalAttack << " damage on " << target->mName <<endl;

	target->mHp -= finalAttack;

	int addMp = rand() % (20 - 10 + 1) + 10; //randomizes mp generated after every attack

	mMp += addMp;

}
