#include <iostream>
#include <string>
#include <time.h>

using namespace std;


int playerBet(int &currentGold)
{
	int betAmount = 0; 

	cout << "Input Bet: ";
	cin >> betAmount; // player inputs bet
	cout << endl;

	cout << "You placed: " << betAmount << endl;

	currentGold = currentGold - betAmount;

	return betAmount;
	
}

void diceRoll(int& dice1, int& dice2) // rolling dice function for both player & computer
{
	dice1 = rand() % 6 + 1;
	dice2 = rand() % 6 + 1;
}

void payOut(int &bet , int& currentGold, int &dice1 , int &dice2 , int &dice3 , int &dice4)
{
	int playerRoll = dice1 + dice2;
	int computerRoll = dice3 + dice4;

	if (playerRoll > computerRoll && playerRoll != 2 && computerRoll != 2) //player high amount no snake eyes WIN
	{
		cout << "Player wins! Receives " << bet << endl;
		currentGold += bet;
		cout << "Player has " << currentGold << endl;
	}
	else if (playerRoll < computerRoll && playerRoll != 2 && computerRoll != 2) //computer high amount no snake eyes LOSE
	{
		cout << "Player loses! Lost " << bet << endl;
		cout << "Player has " << currentGold << endl;
	}
	else if (playerRoll > computerRoll && playerRoll != 2 && computerRoll == 2 ) // player high amount computer snake eyes LOSE
	{
		cout << "SNAKE EYES! Player loses! Lost " << bet << endl;
		cout << "Player has " << currentGold << endl;
	}
	else if (playerRoll < computerRoll && playerRoll == 2 && computerRoll !=2) // computer high amount player snake eyes WIN
	{
		cout << "SNAKE EYES! Player wins! Receives " << bet << endl;
		currentGold += bet * 3;
		cout << "Player has " << currentGold << endl;
	}
	else if (playerRoll > computerRoll && playerRoll == 2 && computerRoll != 2) // player high snakes eyes WIN
	{
		cout << "SNAKE EYES! Player wins! Receives " << bet << endl;
		currentGold += bet * 3;
		cout << "Player has " << currentGold << endl;
	}
	else if (playerRoll < computerRoll && playerRoll != 2 && computerRoll == 2) // computer snakes eyes LOSE
	{
		cout << "SNAKE EYES! Player loses! Lost " << bet << endl;
		cout << "Player has " << currentGold << endl;
	}
	else // DRAW
	{
		cout << "It's a draw!" << endl;
		currentGold += bet;
		cout << "Player has " << currentGold << endl;
	}
}

int playRound(int &currentGold)
{
	if (currentGold <= 0)
	{
		cout << "Player does not have enough gold" << endl;
		cout << "Player is unable to place bet" << endl;

		system("pause");
		system("cls");

		return 0;
	}
	else
	{
		system("pause");
		system("cls");
	}
	 
}

int main()
{
	srand(time(NULL));

	int currentGold = 1000;
	int dice1;
	int dice2;
	int dice3;
	int dice4;
	bool start = true;


	while (start)
	{
		cout << "You have: " << currentGold << " remaining" << endl;
		int bet = playerBet(currentGold);
		
		if (bet > 0 && currentGold >= 0)
		{
			cout << "Current Gold: " << currentGold << endl;

			diceRoll(dice1, dice2);
			cout << "You rolled " << dice1 << " and " << dice2 << endl;

			diceRoll(dice3, dice4);
			cout << "Computer rolled " << dice3 << " and " << dice4 << endl;

			payOut(bet , currentGold, dice1, dice2, dice3, dice4);
			playRound(currentGold);
		}
		else 
		{
			cout << "Invalid Bet" << endl;
			playRound(currentGold);

			system("pause");
			system("cls");

			return 0;

		}
	
	
	}
	

}


