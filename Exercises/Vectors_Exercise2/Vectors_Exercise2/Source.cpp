#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

void inventoryItems(vector<string>& items , vector<string>& inventoryList)
{
	items.push_back("RedPotion");
	items.push_back("Elixir");
	items.push_back("EmptyBottle");
	items.push_back("BluePotion");

	for (int i = 0; i <= 9; i++)
	{
		int randItem = rand() % 4;
		inventoryList.push_back(items[randItem]);
	}
	
}

void displayInventory(vector<string>& items , vector<string>& inventoryList)
{
	for (int i = 0; i < inventoryList.size(); i++)
	{

		cout << inventoryList[i] << endl;

	}
}

void countItem(vector<string>& items, vector<string>& inventoryList)
{
	int redpotion = 0,
		elixir = 0,
		emptybottle = 0,
		bluepotion = 0;

	for (int i = 0; i < inventoryList.size(); i++)
	{
		if (inventoryList[i] == "RedPotion")
		{
			redpotion++;
		}
		else if (inventoryList[i] == "Elixir")
		{
			elixir++;
		}
		else if (inventoryList[i] == "EmptyBottle")
		{
			emptybottle++;
		}
		else if (inventoryList[i] == "BluePotion")
		{
			bluepotion++;
		}

	}

	cout << "There are " << redpotion << " RedPotions in the inventory." << endl;
	cout << "There are " << elixir << " Elixirs in the inventory." << endl;
	cout << "There are " << emptybottle << " EmptyBottles in the inventory." << endl;
	cout << "There are " << bluepotion << " BluePotions in the inventory." << endl;
}

void removeItem(vector<string>& items, vector<string>& inventoryList, string& itemPicked)
{
	int itemFound = -1;

	for (int i = 0; i < inventoryList.size(); i++)
	{
		if (inventoryList[i] == itemPicked)
		{
			itemFound = i;
		}
	}

	if (itemFound != -1)
	{
		inventoryList.erase(inventoryList.begin() + itemFound);

		cout << " " << endl;

		cout << "ITEMS REMAINING IN THE INVENTORY: " << endl;
		displayInventory(items, inventoryList);

		cout << " " << endl;

		cout << "SUMMARY OF REMAINING ITEMS: " << endl;
		countItem(items, inventoryList);


	}
}

int main()
{
	vector<string> items, inventoryList;
	string itemPicked;

	cout << "LOADING ITEMS..." << endl;
	inventoryItems(items, inventoryList);
	system("pause");

	cout << " " << endl;

	cout << "ITEMS IN THE INVENTORY: " << endl;
	displayInventory(items, inventoryList);

	cout << " " << endl;

	cout << "SUMMARY OF ITEMS: " << endl;
	countItem(items, inventoryList);
	system("pause");

	cout << " " << endl;

	cout << "What item would you like to get? " << endl;
	cin >> itemPicked;
	removeItem(items, inventoryList, itemPicked);
	










	/*cout << "What item would you like to remove?" << endl;
	cin >> itemRemove;
	removeItem(items, itemRemove);*/
	
	/*void removeItem(vector<string> & items, string itemRemove)
	{
		int foundItem = -1;
		for (int i = 0; items.size(); i++)
		{
			if (items[i] == itemRemove)
			{
				foundItem = i;
			}
		}

		if (foundItem != -1)
		{
			items.erase(items.begin() + foundItem);
		}

	}*/



}