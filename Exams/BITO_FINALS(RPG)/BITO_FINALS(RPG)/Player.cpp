#include "Player.h"
#include "Unit.h"
#include "Item.h"


using namespace std;

Player::Player(string name)
	:Unit(name)
{
	mXPos = 0;
	mYPos = 0;
	mGold = 0;
	mLevel = 1;
	mExp = 0;
}


void Player::playerRest()
{
	cout << "Player rested. HP is restored to max health." << endl;
	setHp(getMaxHp());
}

void Player::playerMove()
{
	int playerMove = 0;
	cout << "Where would you like to move? " << endl;
	cout << endl;
	cout << "[1] North [2] South [3] East [4] West " << endl;
	cin >> playerMove;

	while (playerMove > 4)
	{
		cout << "Invalid. Please type the correct number." << endl;
		cin >> playerMove;
	}

	while (true)
	{
		if (playerMove == 1) //North
		{
			setYPos(getYPos() + 1);
			break;
		}
		else if (playerMove == 2) //South
		{
			setYPos(getYPos() - 1);
			break;
		}
		else if (playerMove == 3) //East
		{
			setXPos(getXPos() + 1);
			break;
		}
		else if (playerMove == 4) //West
		{
			setXPos(getXPos() - 1);
			break;
		}
	}

}

void Player::playerStats()
{

	cout << "------------------------------------" << endl;
	cout << "PLAYER INFORMATION: " << endl;
	cout << "NAME: " << getName() << endl;
	cout << "CLASS: " << getClass() << endl;
	cout << "GOLD: " << getGold() << endl;
	cout << "LEVEL: " << getLevel() << endl;
	cout << "EXP: " << getExp() << endl;
	cout << "------------------------------------" << endl;
	cout << "PLAYER STATS: " << endl;
	cout << "HP: " << getHp() << endl;
	cout << "POWER: " << getPow() << endl;
	cout << "VITALITY: " << getVit() << endl;
	cout << "AGILITY: " << getAgi() << endl;
	cout << "DEXTERITY: " << getDex() << endl;
	cout << "------------------------------------" << endl;

	system("pause");

}

int Player::getXPos()
{
	return mXPos;
}

int Player::getYPos()
{
	return mYPos;
}

int Player::getGold()
{
	return mGold;
}

int Player::getLevel()
{
	return mLevel;
}

int Player::getExp()
{
	return mExp;
}

void Player::setXPos(int value)
{
	mXPos = value;
}

void Player::setYPos(int value)
{
	mYPos = value;
}

void Player::setGold(int value)
{
	mGold = value;
}

void Player::setLevel(int value)
{
	mLevel = value;
}

void Player::setExp(int value)
{
	mExp = value;
}
