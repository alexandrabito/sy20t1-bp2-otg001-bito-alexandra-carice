#include "Item.h"

Item::Item(string name, int damage, int defense)
{
    mName = name;
    mDamage = damage;
    mDefense = defense;
}

string Item::getName()
{
    return mName;
}

int Item::getDamage()
{
    return mDamage;
}

int Item::getDefense()
{
    return mDefense;
}

void Item::setName(string name)
{
    mName = name;
}

void Item::setDamage(int value)
{
    mDamage = value;
}

void Item::setDefense(int value)
{
    mDefense = value;
}

