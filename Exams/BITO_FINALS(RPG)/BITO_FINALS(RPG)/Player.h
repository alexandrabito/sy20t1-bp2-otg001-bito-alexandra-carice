#pragma once
#include <iostream>
#include <string>
#include <time.h>
#include "Unit.h"

using namespace std;

class Player : public Unit
{
public:
	Player(string name);

	void playerRest();
	void playerMove();
	void playerStats();

	//Getters
	int getXPos();
	int getYPos();
	int getGold();
	int getLevel();
	int getExp();


	//Setters
	void setXPos(int value);
	void setYPos(int value);
	void setGold(int value);
	void setLevel(int value);
	void setExp(int value);


private:
	int mXPos;
	int mYPos;
	int mGold;
	int mLevel;
	int mExp;
};

