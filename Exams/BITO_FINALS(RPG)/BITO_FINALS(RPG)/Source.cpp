#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "Unit.h"
#include "Player.h"
#include "Monster.h"
#include "Item.h"

using namespace std;

class Unit;
class Player;
class Monster;
class Item;

Player* createCharacter(string playerName, int chosenClass, vector<string> playerClass) //Function to create character
{
	cout << "Please input your name: "; //Asks for players name
	cin >> playerName; 
	cout << endl;

	Player* player = new Player(playerName); //Creates new player

	system("cls");

	//Player will choose class
	playerClass.push_back("Warrior");
	playerClass.push_back("Thief");
	playerClass.push_back("Crusader");

	cout << "Hello " << player->getName() << " ! Please choose your class: " << endl;

	for (int i = 0; i < playerClass.size(); i++)
	{
		cout << "	[" << i << "]" << playerClass[i] << endl;
	}

	cin >> chosenClass;

	while (chosenClass > 2) //When player inputs an invalid number
	{
		cout << "Please input a valid number." << endl;
		cin >> chosenClass;
	}

	if (chosenClass == 0)
	{
		player->setClass("Warrior"); //If player chooses '0'
		player->setHp(100);
		player->setMaxHp(100);
		player->setPow(15);
		player->setVit(5);
		player->setAgi(5);
		player->setDex(5);

		Item* weapon = new Item("Iron Sword" , 3 , 0);
		Item* armor = new Item("Shirt" , 0 , 1);

		player->setWeapon(weapon);
		player->setArmor(armor);
	}
	else if (chosenClass == 1)
	{
		player->setClass("Thief"); //If player chooses '1'
		player->setHp(100);
		player->setMaxHp(100);
		player->setPow(5);
		player->setVit(5);
		player->setAgi(12);
		player->setDex(12);

		Item* weapon = new Item("Wooden Dagger", 1, 0);
		Item* armor = new Item("Shirt", 0, 1);

		player->setWeapon(weapon);
		player->setArmor(armor);

	}
	else
	{
		player->setClass("Crusader"); //If player chooses '2'
		player->setHp(115);
		player->setMaxHp(115);
		player->setPow(5);
		player->setVit(12);
		player->setAgi(5);
		player->setDex(5);
		
		Item* weapon = new Item("Wooden Lance", 1, 0);
		Item* armor = new Item("Shirt", 0, 1);

		player->setWeapon(weapon);
		player->setArmor(armor);
	}

	system("pause");
	system("cls");


	return player;
}

Monster* monsterSpawner( int randomMonsterClass) //Spawns enemy
{
	randomMonsterClass = rand() % ((100 - 1 + 1) + 1);

	if (randomMonsterClass <= 31 && randomMonsterClass >= 1)
	{
		Monster* monster = new Monster("Goblin");
		monster->setHp(30);
		monster->setMaxHp(30);
		monster->setPow(rand() % ((10 - 5 + 1) + 5));
		monster->setVit(rand() % ((10 - 5 + 1) + 5));
		monster->setAgi(rand() % ((10 - 5 + 1) + 5));
		monster->setDex(rand() % ((10 - 5 + 1) + 5));

		Item* weapon = new Item("Stick", 0, 0);
		Item* armor = new Item("Rags", 0, 0);

		monster->setWeapon(weapon);
		monster->setArmor(armor);

		return monster;
	}
	else if (randomMonsterClass <= 62 && randomMonsterClass >= 32)
	{
		Monster* monster = new Monster("Ogre");
		monster->setHp(40);
		monster->setMaxHp(40);
		monster->setPow(rand() % ((15 - 10 + 1) + 10));
		monster->setVit(rand() % ((15 - 10 + 1) + 10));
		monster->setAgi(rand() % ((15 - 10 + 1) + 10));
		monster->setDex(rand() % ((15 - 10 + 1) + 10));

		Item* weapon = new Item("Muddy Log", 0, 0);
		Item* armor = new Item("Muddy Shirt and Pants", 0, 1);

		monster->setWeapon(weapon);
		monster->setArmor(armor);
		
		return monster;
	}
	else if (randomMonsterClass <= 94 && randomMonsterClass >= 63)
	{
		Monster* monster = new Monster("Orc");
		monster->setHp(50);
		monster->setMaxHp(50);
		monster->setPow(rand() % ((25 - 20 + 1) + 20));
		monster->setVit(rand() % ((25 - 20 + 1) + 20));
		monster->setAgi(rand() % ((25 - 20 + 1) + 20));
		monster->setDex(rand() % ((25 - 20 + 1) + 20));

		Item* weapon = new Item("Tree Trunk", 0, 0);
		Item* armor = new Item("Stolen Armor", 0, 2);

		monster->setWeapon(weapon);
		monster->setArmor(armor);

		return monster;
	}
	else if (randomMonsterClass >= 95 && randomMonsterClass <= 100)
	{
		Monster* monster = new Monster("Orc Lord");
		monster->setHp(65);
		monster->setMaxHp(65);
		monster->setPow(rand() % ((45 - 35 + 1) + 35));
		monster->setVit(rand() % ((45 - 35 + 1) + 35));
		monster->setAgi(rand() % ((45 - 35 + 1) + 35));
		monster->setDex(rand() % ((45 - 35 + 1) + 35));

		Item* weapon = new Item("Giant Boulder", 0, 0);
		Item* armor = new Item("Cursed Armor", 0, 4);

		monster->setWeapon(weapon);
		monster->setArmor(armor);

		return monster;
	}

}

void buyWeapon(Player* player, Item* weapon)
{
	int purchaseWeapon;

	while (true)
	{
		cout << "[1] Dull Blade | 4 Damage | 25 Gold" << endl;
		cout << "[2] Sacrificial Sword | 25 Damage | 500 Gold" << endl;
		cout << "[3] Cursed Sword | 1000 Damage | 350000 Gold" << endl;
		cout << "[4] Exit " << endl;

		cin >> purchaseWeapon;

		if (purchaseWeapon == 1)
		{
			if (player->getGold() > 25)
			{
				player->setGold(player->getGold() - 25);
				delete weapon;
				Item* weapon = new Item("Dull Blade", 4, 0);
				player->setWeapon(weapon);
				cout << "You purchased a " << player->getWeapon()->getName() << " !" << endl;

				cout << endl;
				system("pause");
			}
			else if (player->getGold() < 25)
			{
				cout << "Not enough gold!" << endl;

				cout << endl;
				system("pause");
			}
		}
		else if (purchaseWeapon == 2)
		{
			if (player->getGold() > 500)
			{
				player->setGold(player->getGold() - 500);
				delete weapon;
				Item* weapon = new Item("Sacrificial Sword", 25, 0);
				player->setWeapon(weapon);
				cout << "You purchased a " << player->getWeapon()->getName() << " !" << endl;

				cout << endl;
				system("pause");
			}
			else if (player->getGold() < 500)
			{
				cout << "Not enough gold!" << endl;

				cout << endl;
				system("pause");
			}
		}
		else if (purchaseWeapon == 3)
		{
			if (player->getGold() > 350000)
			{
				player->setGold(player->getGold() - 350000);
				delete weapon;
				Item* weapon = new Item("Cursed Sword", 1000, 0);
				player->setWeapon(weapon);
				cout << "You purchased a " << player->getWeapon()->getName() << " !" << endl;

				cout << endl;
				system("pause");
			}
			else if (player->getGold() < 350000)
			{
				cout << "Not enough gold!" << endl;

				cout << endl;
				system("pause");
			}
		}
		else if (purchaseWeapon == 4)
		{
			break;
		}

		while (purchaseWeapon > 4)  //When player inputs an invalid number
		{
			cout << "Please input a valid number." << endl;
			cin >> purchaseWeapon;
		}
	}
}

void buyArmor(Player* player, Item* armor)
{
	int purchaseArmor;

	while (true)
	{
		cout << "[1] Bone Armor | 15 Defense | 85 Gold" << endl;
		cout << "[2] Diamond Armor | 55 Defense | 500 Gold" << endl;
		cout << "[3] Enchanted Armor | 100 Defense | 25000 Gold" << endl;
		cout << "[4] Exit " << endl;

		cin >> purchaseArmor;

		if (purchaseArmor == 1)
		{
			if (player->getGold() > 85)
			{
				player->setGold(player->getGold() - 85);
				delete armor;
				Item* armor = new Item("Bone Armor", 0, 15);
				player->setArmor(armor);
				cout << "You purchased a " << player->getArmor()->getName() << " !" << endl;

				cout << endl;
				system("pause");
			}
			else if (player->getGold() < 85)
			{
				cout << "Not enough gold!" << endl;

				cout << endl;
				system("pause");
			}
		}
		else if (purchaseArmor == 2)
		{
			if (player->getGold() > 500)
			{
				player->setGold(player->getGold() - 500);
				delete armor;
				Item* armor = new Item("Diamond Armor", 0, 55);
				player->setArmor(armor);
				cout << "You purchased a " << player->getArmor()->getName() << " !" << endl;

				cout << endl;
				system("pause");
			}
			else if (player->getGold() < 500)
			{
				cout << "Not enough gold!" << endl;

				cout << endl;
				system("pause");
			}
		}
		else if (purchaseArmor == 3)
		{
			if (player->getGold() > 25000)
			{
				player->setGold(player->getGold() - 25000);
				delete armor;
				Item* armor = new Item("Enchanted Armor", 0, 100);
				player->setArmor(armor);
				cout << "You purchased a " << player->getArmor()->getName() << " !" << endl;

				cout << endl;
				system("pause");
			}
			else if (player->getGold() < 25000)
			{
				cout << "Not enough gold!" << endl;

				cout << endl;
				system("pause");
			}
		}
		else if (purchaseArmor == 4)
		{
			break;
		}

		while (purchaseArmor > 4)  //When player inputs an invalid number
		{
			cout << "Please input a valid number." << endl;
			cin >> purchaseArmor;
		}
	}
}

void combat(Player* player, Monster* monster) //Combat and declaring of winner
{
	int playerDecision;

	cout << "You encountered a " << monster->getName() << endl;
	monster->monsterStats();

	while (player->getHp() > 0 && monster->getHp() > 0)
	{
		cout << "What will you like to do?" << endl;
		cout << endl;
		cout << "[1] Attack [2] Run Away" << endl;
		cin >> playerDecision;

		if (playerDecision == 1)
		{
			player->attack(monster);
			if (monster->getHp() != 0)
			{
				monster->attack(player);
			}
			
		}
		else if (playerDecision == 2)
		{
			int randRun = rand() % 25;

			if (randRun >= rand() % 25)
			{
				cout << "You have successfully ran away!" << endl;
				delete monster;
				break;
			}
			else if (randRun < rand() % 25)
			{
				cout << "You tried to run away but failed!" << endl;
			}
		}
	}

	if (player->getHp() > 0 && monster->getHp() == 0)
	{
		cout << "You have defeated the " << monster->getName() << " !" << endl;

		if (monster->getName() == "Goblin")
		{
			cout << "You have received 100 xp and 10 Gold" << endl;
			player->setGold(player->getGold() + 10);
			player->setExp(player->getExp() + 100);

			delete monster;
		}
		else if (monster->getName() == "Ogre")
		{
			cout << "You have received 250 xp and 50 Gold" << endl;
			player->setGold(player->getGold() + 50);
			player->setExp(player->getExp() + 250);

			delete monster;
		}
		else if (monster->getName() == "Orc")
		{
			cout << "You have received 500 xp and 100 Gold" << endl;
			player->setGold(player->getGold() + 100);
			player->setExp(player->getExp() + 500);

			delete monster;
		}
		else if (monster->getName() == "Orc Lord")
		{
			cout << "You have received 1000 xp and 1000 Gold" << endl;
			player->setGold(player->getGold() + 1000);
			player->setExp(player->getExp() + 1000);

			delete monster;
		}

		
	}

	if (player->getHp() == 0 && monster->getHp() > 0)
	{
		cout << "You have been defeated." << endl;
	}
	
	system("pause");
	system("cls");
}

void levelUp(Player* player) //Player levels up
{
	cout << "Player Leveled Up!" << endl;
	player->setLevel(player->getLevel() + 1);

	int randStat = rand() % 5,
		statGiver = rand() % 4 + 1;

	if (statGiver == 1)
	{
		player->setPow(player->getPow() + randStat);

		cout << "Player received +" << randStat << " for Power!" << endl;
	}
	else if (statGiver == 2)
	{
		player->setVit(player->getVit() + randStat);

		cout << "Player received +" << randStat << " for Vitality!" << endl;
	}
	else if (statGiver == 2)
	{
		player->setAgi(player->getAgi() + randStat);

		cout << "Player received +" << randStat << " for Agility!" << endl;
	}
	else
	{
		player->setDex(player->getDex() + randStat);

		cout << "Player received +" << randStat << " for Dexterity!" << endl;
	}

	system("pause");
	system("cls");

	
}

int main()
{
	//Variables 
	string playerName;
	int chosenClass = 0,
		randomMonsterClass = 0,
		playerInput = 0,
		playerChoice = 0;
		
	vector<string> playerClass;

	Item* weapon = 0;
	Item* armor = 0;

	Player* player = createCharacter(playerName, chosenClass, playerClass);

	int requiredExp = player->getLevel() * 1000;

	while (player->getHp() > 0)
	{
		cout << "Current Location: " << player->getXPos() << " , " << player->getYPos() << endl; //Displays current location
		cout << endl;
		cout << "What would you like to do? " << endl;
		cout << endl;
		cout << "[1] Move [2] Rest [3] Display Stats [4] Quit" << endl;
		cin >> playerInput;
		cout << endl;

		while (playerInput > 4)
		{
			cout << "Invalid. Please type the correct number." << endl;
			cin >> playerInput;
		}

		if (playerInput == 1) //Player will move
		{
			player->playerMove();

			if (player->getXPos() == 1 && player->getYPos() == 1) //Enters black market at 1,1
			{
				while (true)
				{
					cout << "Welcome to the Black Market! What can I do for you?" << endl;
					cout << endl;
					cout << "[1] Buy Weapon [2] Buy Armor [3] Exit " << endl;
					cin >> playerChoice;

					if (playerChoice == 1)
					{
						buyWeapon(player, weapon);
					}
					else if (playerChoice == 2)
					{
						buyArmor(player, armor);
					}
					else
					{
						break;
					}
				}
			}
			else
			{
				int enemySpawn = rand() % ((100 + 20 + 1) + 20);

				if (enemySpawn > 20)
				{
					Monster* monster = monsterSpawner(randomMonsterClass);
					combat(player, monster);
					
					if (player->getExp() == requiredExp)
					{
						levelUp(player);
					}
				}
				else
				{
					cout << "No monster in sight! Continue exploring..." << endl;
				}
			}
		}
		else if (playerInput == 2) //Player will rest
		{
			player->playerRest();
		}
		else if (playerInput == 3) //Display Stats
		{
			player->playerStats();
		}
		else if (playerInput == 4) //Quit
		{
			delete player; 
			break;
		}
	}


	



}