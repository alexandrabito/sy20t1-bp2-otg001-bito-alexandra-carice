#pragma once
#include <string>
#include <time.h>


using namespace std;

class Item;

class Unit
{
public:
	Unit(string name);

	//Behaviors
	void attack(Unit* target);

	//Getters
	string getName();
	string getClass();
	Item* getWeapon();
	Item* getArmor();
	int getHp();
	int getMaxHp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	//Setters
	void setName(string name);
	void setClass(string unitClass);
	void setWeapon(Item* weapon);
	void setArmor(Item* armor);
	void setHp(int value);
	void setMaxHp(int value);
	void setPow(int value);
	void setVit(int value);
	void setAgi(int value);
	void setDex(int value);

private:
	string mName;
	string mClass;
	Item* mWeapon;
	Item* mArmor;
	int mHp;
	int mMaxHp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;

};

