#pragma once
#include <string>

using namespace std;

class Item
{
public:
	Item(string name, int damage, int defense);

	//Getters
	string getName();
	int getDamage();
	int getDefense();

	//Setters
	void setName(string name);
	void setDamage(int value);
	void setDefense(int value);

private:
	string mName;
	int mDamage;
	int mDefense;
};

