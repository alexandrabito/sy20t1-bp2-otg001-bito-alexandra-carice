
#include "Unit.h"
#include "Player.h"
#include "Monster.h"
#include "Item.h"

Unit::Unit(string name)
{
    mName = name;
}

void Unit::attack(Unit* target)
{
    int unitDamage = 0,
        unitHitRate = 0;

    unitDamage = ((mPow + getWeapon()->getDamage()) - (target->getVit() - getArmor()->getDefense()));

    if (unitDamage <= 0)
    {
        unitDamage = 1;
    }

    unitHitRate = (mDex / target->getAgi()) * 100;

    if (unitHitRate > 80)
    {
        unitHitRate = 80;
    }
    else if (unitHitRate < 20)
    {
        unitHitRate = 20;
    }

    cout << mName << " is attacking..." << endl;
    cout << endl;
    system("pause");

    if (unitHitRate > rand() % 100)
    {
        target->setHp(target->getHp() - unitDamage);
        cout << mName << " attacked " << target->getName() << " and caused " << unitDamage << " damage!" << endl;
    }
    else
    {
        cout << mName << " missed!" << endl;
    }

    cout << endl;
    system("pause");
}

string Unit::getName()
{
    return mName;
}

string Unit::getClass()
{
    return mClass;
}

Item* Unit::getWeapon()
{
    return mWeapon;
}

Item* Unit::getArmor()
{
    return mArmor;
}

int Unit::getHp()
{
    return mHp;
}

int Unit::getMaxHp()
{
    return mMaxHp;
}

int Unit::getPow()
{
    return mPow;
}

int Unit::getVit()
{
    return mVit;
}

int Unit::getAgi()
{
    return mAgi;
}

int Unit::getDex()
{
    return mDex;
}

void  Unit::setName(string name)
{
    mName = name;
}

void Unit::setClass(string unitClass)
{
    mClass = unitClass;
}

void  Unit::setWeapon(Item* weapon)
{
    mWeapon = weapon;
}

void Unit::setArmor(Item* armor)
{
    mArmor = armor;
}

void Unit::setHp(int value)
{
    mHp = value;
    if (mHp < 0) mHp = 0;
}

void Unit::setMaxHp(int value)
{
    mMaxHp = value;
}

void Unit::setPow(int value)
{
    mPow = value;
}

void Unit::setVit(int value)
{
   mVit = value;
}

void Unit::setAgi(int value)
{
    mAgi = value;
}

void Unit::setDex(int value)
{
    mDex = value;
}
