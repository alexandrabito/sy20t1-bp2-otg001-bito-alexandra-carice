#include "Monster.h"

Monster::Monster(string name)
	:Unit(name)
{
	
}

void Monster::monsterStats()
{
	cout << "------------------------------------" << endl;
	cout << "MONSTER'S INFORMATION: " << endl;
	cout << "NAME: " << getName() << endl;
	cout << "HP: " << getHp() << endl;
	cout << "POWER: " << getPow() << endl;
	cout << "VITALITY: " << getVit() << endl;
	cout << "AGILITY: " << getAgi() << endl;
	cout << "DEXTERITY: " << getDex() << endl;
	cout << "------------------------------------" << endl;

	system("pause");

}
