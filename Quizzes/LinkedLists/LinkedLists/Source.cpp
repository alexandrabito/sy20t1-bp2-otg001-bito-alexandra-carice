#include <iostream>
#include <string>
#include <time.h>
#include "Node.h"

using namespace std;

void createList(Node* n1, Node* n2, Node* n3, Node* n4, Node* n5) //Function that makes the list circular
{
	n1->next = n2;
	n2->next = n3;
	n3->next = n4;
	n4->next = n5;
	n5->next = n1;

	n1->previous = n5;
	n2->previous = n1;
	n3->previous = n2;
	n4->previous = n3;
	n5->previous = n4;

	system("pause");
	system("cls");
}

void displaySoldiers(Node*& current, int& count) // Prints the names on the list
{
	cout << "List of soldiers are:" << endl;
	cout << "==============================" << endl;

	for (int i = 1; i <= count; i++)
	{
		cout << current->name << endl;
		current = current->next;
	}

	system("pause");
}

void selectStart(Node*& current, int& count, int& randRoll) // Determining starting point
{
	cout << "==============================" << endl;
	cout << "The Lord Commander randomly rolls a number..." << endl;
	cout << endl;

	system("pause");

	randRoll = rand() % count + 1;

	cout << "They rolled a " << randRoll << endl;

	for (int i = 1; i < randRoll ; i++)
	{
		current = current->next;
	}

	cout << "The Lord Commander selects " << current->name << endl;

	for (int i = 1; i <= count; i++)
	{
		cout << current->name << endl;
		current = current->next;
	}

	cout << endl;
	system("pause");
}

void soldierRoll(Node*& current, int& count, int& randRoll, int& randSoldier)
{
	cout << current->name << " randomly picks a number..." << endl;
	cout << endl;

	randSoldier = rand() % count + 1;

	/*cout << "Number picked: " << randSoldier << endl;*/

	cout << "The cloak is passed " << randSoldier << " time/s" << endl;

	//for (int i = 1; i <= randSoldier; i++)
	//{
	//	current = current->next;
	//}

	cout << endl;


	Node* toDelete = current; // Value or character to be deleted
	Node* previous1 = nullptr;

	for (int i = 0; i < randSoldier; i++)
	{
		previous1 = toDelete;
		toDelete = toDelete->next;
	}

	previous1->next = toDelete->next;

	cout << toDelete->name << " is eliminated" << endl;

	delete toDelete;
	toDelete = nullptr;
	current = previous1->next;
	count--;

	
	system("pause");

}

int main()
{
	srand(time(NULL));

	//Variables
	string input; // given name by user

	int count = 0,
		randRoll = 0,
		randSoldier = 0;

	//Populating List

	Node* n1 = new Node;
	cout << "What is your name soldier? ";
	cin >> input;
	n1->name = input;
	count++;

	Node* n2 = new Node;
	cout << "What is your name soldier? ";
	cin >> input;
	n2->name = input;
	count++;

	Node* n3 = new Node;
	cout << "What is your name soldier? ";
	cin >> input;
	n3->name = input;
	count++;

	Node* n4 = new Node;
	cout << "What is your name soldier? ";
	cin >> input;
	n4->name = input;
	count++;

	Node* n5 = new Node;
	cout << "What is your name soldier? ";
	cin >> input;
	n5->name = input;
	count++;

	Node* head = n1;
	Node* current = head;


	createList(n1, n2, n3, n4, n5);
	displaySoldiers(current, count);
	selectStart(current, count, randRoll);
	
	while (count != 1)
	{
		soldierRoll(current, count, randRoll, randSoldier);
		displaySoldiers(current, count);
	}

	cout << current->name << " is the last man standing. " << current->name << " will go to seek for reinforcements." << endl;
}
