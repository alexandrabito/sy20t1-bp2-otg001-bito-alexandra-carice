#include "Player.h"
#include "Enemy.h"

class Enemy;

void Player::attack(Enemy* target)
{
	int playerDamage = 0,
		playerHitRate = 0;

	//Determining if Player is stronger/ weaker than Enemy
	if (mClass == "Warrior" && target->getClass() == "Assassin")
	{
		playerDamage = (mPower - target->getVitality()) * 1.5;
	}
	else if (mClass == "Assassin" && target->getClass() == "Mage")
	{
		playerDamage = (mPower - target->getVitality()) * 1.5;
	}
	else if (mClass == "Mage" && target->getClass() == "Warrior")
	{
		playerDamage = (mPower - target->getVitality()) * 1.5;
	}
	else
	{
		playerDamage = (mPower - target->getVitality()) * 1;
	}

	//If radomizer sets player damage to 0 then damage is set to 1
	if (playerDamage <= 0)
	{
		playerDamage = 1;
	}

	//Determining if attack is a hit or a miss
	playerHitRate = (mDexterity / (float)target->getAgility()) * 100;

	//Setting Limits to the hit rate
	if (playerHitRate > 80)
	{
		playerHitRate = 80;
	}
	else if (playerHitRate < 20)
	{
		playerHitRate = 20;
	}

	cout << mName << " is preparing to attack..." << endl;
	cout << endl;
	system("pause");
	
	if (playerHitRate > rand() % 100) //If hit rate is higher than random number then player attacks if not its a miss
	{
		target->setHp(target->getHp() - playerDamage);
		cout << mName << " attacked " << target->getName() << " and caused " << playerDamage << " damage!" << endl;
	}
	else
	{
		cout << mName << " missed!" << endl;
	}

	cout << endl;
	system("pause");
}

string Player::getName()
{
	return mName;
}

string Player::getClass()
{
	return mClass;
}

int Player::getHp()
{
	return mHp;
	if (mHp < 0) mHp = 0;
}

int Player::getPower()
{
	return mPower;
}

int Player::getVitality()
{
	return mVitality;
}

int Player::getAgility()
{
	return mAgility;
}

int Player::getDexterity()
{
	return mDexterity;
}

void Player::setName(string name)
{
	mName = name;
}

void Player::setClass(string className)
{
	mClass = className;
}

void Player::setHp(int value)
{
	mHp = value;
	if (mHp < 0) mHp = 0;
}

void Player::setPower(int value)
{
	mPower = value;
}

void Player::setVitality(int value)
{
	mVitality = value;
}

void Player::setAgility(int value)
{
	mAgility = value;
}

void Player::setDexterity(int value)
{
	mDexterity = value;
}
