#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include "Player.h"
#include "Enemy.h"

using namespace std;

//Class
class Player;
class Enemy;

void createCharacter(Player*& player,vector<string>& playerClass, string& playerName, int& chosenClass)
{
	//Player inputs name
	cout << "Input player name: ";
	cin >> playerName;

	player->setName(playerName);

	system("cls");

	//Player chooses class
	playerClass.push_back("Warrior");
	playerClass.push_back("Assassin");
	playerClass.push_back("Mage");


	cout << "Choose your class: " << endl;

	for (int i = 0; i < playerClass.size(); i++)
	{
		cout << "	[" << i << "]" << playerClass[i] << endl;
	}
	
	
	cin >> chosenClass; 

	while (chosenClass > 2) //When player inputs an invalid number
	{
		cout << "Please input a valid number." << endl; 
		cin >> chosenClass;
	}
	
	if (chosenClass == 0)
	{
		player->setClass("Warrior"); //If player chooses '0'
	}
	else if (chosenClass == 1)
	{
		player->setClass("Assassin"); //If player chooses '1'
	}
	else 
	{
		player->setClass("Mage"); //If player chooses '2'
	}

	system("cls");
}

void createEnemy(Enemy*& enemy, vector<string> enemyClass, int& randomEnemyClass) //Function that creates enemy's class and name)
{
	enemyClass.push_back("Warrior");
	enemyClass.push_back("Assassin");
	enemyClass.push_back("Mage");

	for (int i = 0; i < enemyClass.size(); i++)
	{
		enemyClass[i];
	}

	randomEnemyClass = rand() % 2 + 1;

	enemy->setClass(enemyClass[randomEnemyClass]);


	if (enemy->getClass() == "Warrior")
	{
		enemy->setName("Enemy Warrior");
	}
	else if (enemy->getClass() == "Assassin")
	{
		enemy->setName("Enemy Assassin");
	}
	else
	{
		enemy->setName("Enemy Mage");
	}

	
	
}

void enemyStats(Enemy* enemy, int hpRandomizer2, int maximumHpNumber2, int minimumHpNumber2, int statRandomizer2, int maximumStatNumber2, int minimumStatNumber2) //Sets enemy's stats
{
	hpRandomizer2 = rand() % (maximumHpNumber2 - minimumHpNumber2 + 1) + minimumHpNumber2; //Randomize HP of enemy
	enemy->setHp(hpRandomizer2); //Sets enemy's hp

	statRandomizer2 = rand() % (maximumStatNumber2 - minimumStatNumber2 + 1) + minimumStatNumber2; //Randomize Power of enemy
	enemy->setPower(statRandomizer2);

	statRandomizer2 = rand() % (maximumStatNumber2 - minimumStatNumber2 + 1) + minimumStatNumber2;//Randomize Vitality of enemy
	enemy->setVitality(statRandomizer2);

	statRandomizer2 = rand() % (maximumStatNumber2 - minimumStatNumber2 + 1) + minimumStatNumber2;//Randomize Agility of enemy
	enemy->setAgility(statRandomizer2);

	statRandomizer2 = rand() % (maximumStatNumber2 - minimumStatNumber2 + 1) + minimumStatNumber2;//Randomize Dexterity of enemy
	enemy->setDexterity(statRandomizer2);
}

void enemyEncounter(Player*& player, Enemy*& enemy) //Player VS Enemy
{
	//Determining who attacks first
	if (player->getAgility() < enemy->getAgility())
	{
		enemy->attack(player);
		player->attack(enemy);

		system("cls");
	}
	else
	{
		player->attack(enemy);
		enemy->attack(player);

		system("cls");
	}
}

void strongerEnemies(Enemy* enemy, int& statBoost) //Will boost stats of upcoming enemies and provide stronger opponents for player
{
	enemy->setHp(enemy->getHp() + statBoost);
	enemy->setPower(enemy->getPower() + statBoost);
	enemy->setVitality(enemy->getVitality() + statBoost);
	enemy->setAgility(enemy->getAgility() + statBoost);
	enemy->setDexterity(enemy->getDexterity() + statBoost);
}

void victoryStatus(Player*& player, Enemy*& enemy, int& round, int& heal, int& enemiesDefeated, const int hpRandomizer) // Will output the victory status of player when their HP reaches 0
{
	if (player->getHp() > 0 && enemy->getHp() == 0)//Win
	{
		cout << player->getName() << " has defeated " << enemy->getName() << endl;

		//Adding Stat Bonus each win
		if (player->getClass() == "Warrior")
		{
			player->setPower(player->getPower() + 3);
			player->setVitality(player->getVitality() + 3);
		}
		else if (player->getClass() == "Assassin")
		{
			player->setAgility(player->getAgility() + 3);
			player->setDexterity(player->getDexterity() + 3);
		}
		else if (player->getClass() == "Mage")
		{
			player->setPower(player->getPower() + 5);
		}

		round++; // Increment for rounds survived
		enemiesDefeated++; //Counts enemies defeated

		heal = hpRandomizer * 0.3; //Heal player's hp by 30% of it's remaining hp

		if (player->getAgility() < enemy->getAgility()) 
		{
			player->setHp(player->getHp() + heal - enemy->getAttackDamage()); //Will subtract attack damage since enemy attacks first
		}
		else
		{
			player->setHp(player->getHp() + heal);
		}
	}
	else if (player->getHp() == 0 && enemy->getHp() > 0) //Lose
	{
		cout << player->getName() << " has been defeated. " << endl;
		cout << "They have survived " << round << " rounds and defeated " << enemiesDefeated << " enemies" << endl;
	}
	else //Tie
	{
		cout << player->getName() << " has been defeated. " << endl;
		cout << "They have survived " << round << " rounds and defeated " << enemiesDefeated << " enemies" << endl;
	}
}

int main()
{
	srand(time(NULL));

	//Variables
	string playerName;
	
		//For player
	int chosenClass = 0, //Chosen class of player
		hpRandomizer = 0,
		statRandomizer = 0,
		maximumHpNumber = 20, //Initial Max Number of Hp
		minimumHpNumber = 10, //Initital Minimum Number of Hp
		maximumStatNumber = 10, //Initial Max Number of Stats
		minimumStatNumber = 5,  //Initital Minimum Number of Stats

		//For enemy
		randomEnemyClass = 0, //Enemy class
		hpRandomizer2 = 0,
		statRandomizer2 = 0,
		maximumHpNumber2 = 15, //Initial Max Number of Hp
		minimumHpNumber2 = 5, //Initital Minimum Number of Hp
		maximumStatNumber2 = 10, //Initial Max Number of Stats
		minimumStatNumber2 = 5,  //Initital Minimum Number of Stats
		statBoost = 3,

		//For counting rounds/ how many opponents defeated
		heal = 0,
		round = 1,
		turn = 1,
		enemiesDefeated = 0;

	//Vectors
	vector<string> playerClass;
	vector<string> enemyClass;

	//Creating the player's character & distributing stats
	Player* player = new Player();
	createCharacter(player, playerClass, playerName, chosenClass); //Function where player inputs name and chooses class

	hpRandomizer = rand() % (maximumHpNumber - minimumHpNumber + 1) + minimumHpNumber; //Randomize HP
	player->setHp(hpRandomizer); //Sets player's hp

	statRandomizer = rand() % (maximumStatNumber - minimumStatNumber + 1) + minimumStatNumber; //Randomize Power
	player->setPower(statRandomizer);

	statRandomizer = rand() % (maximumStatNumber - minimumStatNumber + 1) + minimumStatNumber;//Randomize Vitality
	player->setVitality(statRandomizer);

	statRandomizer = rand() % (maximumStatNumber - minimumStatNumber + 1) + minimumStatNumber;//Randomize Agility
	player->setAgility(statRandomizer);

	statRandomizer = rand() % (maximumStatNumber - minimumStatNumber + 1) + minimumStatNumber;//Randomize Dexterity
	player->setDexterity(statRandomizer);

	//Creating Enemy
	Enemy* enemy = new Enemy(); //Create new enemy
	createEnemy(enemy, enemyClass, randomEnemyClass); //Create an enemy function- determines the class of enemy
	enemyStats(enemy, hpRandomizer2, maximumHpNumber2, minimumHpNumber2, statRandomizer2, maximumStatNumber2, minimumStatNumber2); //Sets enemy stats

	//Combat and Displaying of Information per round 
	while (player->getHp() != 0)
	{

		//Displays round
		cout << "Round: " << round << endl; 
		cout << "Turn: " << turn << endl;
		cout << "====================================" << endl;

		cout << endl;

		//Display Player's Information
		cout << "PLAYER'S INFORMATION" << endl;
		cout << "NAME: " << player->getName() << endl;
		cout << "CLASS: " << player->getClass() << endl;
		cout << "HP: " << player->getHp() << endl;
		cout << "POWER: " << player->getPower() << endl;
		cout << "VITALITY: " << player->getVitality() << endl;
		cout << "AGILITY: " << player->getAgility() << endl;
		cout << "DEXTERITY: " << player->getDexterity() << endl;

		
		cout << endl;

		//Display Enemy's Information
		cout << "ENEMY'S INFORMATION" << endl;
		cout << "NAME: " << enemy->getName() << endl;
		cout << "CLASS: " << enemy->getClass() << endl;
		cout << "HP: " << enemy->getHp() << endl;
		cout << "POWER: " << enemy->getPower() << endl;
		cout << "VITALITY: " << enemy->getVitality() << endl;
		cout << "AGILITY: " << enemy->getAgility() << endl;
		cout << "DEXTERITY: " << enemy->getDexterity() << endl;

		cout << endl;
		cout << "Initiating Combat..." << endl;
		system("pause");
		system("cls");

		
		enemyEncounter(player, enemy); //Display Combat with Enemy, function that determines who attacks first based on Agility
		turn++;

		cout << endl;


		if (player->getHp() > 0 && enemy->getHp() == 0)
		{
			/*cout << enemy->getName() << " has been defeated!" << endl;
			system("pause");
			system("cls");

			round++;
			enemiesDefeated++;*/ //Increment for enemies defeated

			victoryStatus(player, enemy, round, heal, enemiesDefeated,hpRandomizer);
			createEnemy(enemy, enemyClass, randomEnemyClass); //Create an enemy function- determines the class of enemy
			enemyStats(enemy, hpRandomizer2, maximumHpNumber2, minimumHpNumber2, statRandomizer2, maximumStatNumber2, minimumStatNumber2); //Sets enemy stats
			strongerEnemies(enemy, statBoost); //Function that creates stronger enemies
		}
		
		if (player->getHp() == 0)
		{
			delete enemy;
			victoryStatus(player, enemy, round, heal, enemiesDefeated,hpRandomizer);//Will displaye status of player when defeated 
			break;
		}

		system("pause");
		
	}
	

}

//while (player->getHp() != 0 || enemy->getHp() != 0) //Player vs Enemy
	