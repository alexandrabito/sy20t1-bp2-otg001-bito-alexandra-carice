#pragma once
#include <iostream>
#include <string>
#include "Player.h"
#include "Enemy.h"

using namespace std;

class Player;

class Enemy
{
public:
	//Methods
	void attack(Player* target);

	//Getters
	string getName();
	string getClass();
	int getHp();
	int getPower();
	int getVitality();
	int getAgility();
	int getDexterity();
	int getAttackDamage();

	//Setters
	void setName(string name);
	void setClass(string className);
	void setHp(int value);
	void setPower(int value);
	void setVitality(int value);
	void setAgility(int value);
	void setDexterity(int value);
	void setAttackDamage(int value);



private:
	//Data Members
	string mName;
	string mClass;
	int mHp;
	int mPower;
	int mVitality;
	int mAgility;
	int mDexterity;
	int mAttackDamage;

};

