#pragma once
#include <iostream>
#include <string>
#include <time.h>
#include "Player.h"
#include "Enemy.h"

using namespace std;

class Enemy;

class Player
{
public:
	//Methods
	void attack(Enemy* target);

	//Getters
	string getName();
	string getClass();
	int getHp();
	int getPower();
	int getVitality();
	int getAgility();
	int getDexterity();

	//Setters
	void setName(string name);
	void setClass(string className);
	void setHp(int value);
	void setPower(int value);
	void setVitality(int value);
	void setAgility(int value);
	void setDexterity(int value);

	

private:
	//Data Members
	string mName;
	string mClass;
	int mHp;
	int mPower;
	int mVitality;
	int mAgility;
	int mDexterity;

};

