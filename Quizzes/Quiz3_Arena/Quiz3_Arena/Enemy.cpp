#include "Player.h"
#include "Enemy.h"

class Player;

void Enemy::attack(Player* target)
{
	int enemyDamage = 0,
		enemyHitRate = 0;

	if (mClass == "Warrior" && target->getClass() == "Assassin")
	{
		enemyDamage = (mPower - target->getVitality()) * 1.5;
	}
	else if (mClass == "Assassin" && target->getClass() == "Mage")
	{
		enemyDamage = (mPower - target->getVitality()) * 1.5;
	}
	else if (mClass == "Mage" && target->getClass() == "Warrior")
	{
		enemyDamage = (mPower - target->getVitality()) * 1.5;
	}
	else
	{
		enemyDamage = (mPower - target->getVitality()) * 1;
	}

	if (enemyDamage <= 0)
	{
		enemyDamage = 1;
	}

	enemyHitRate = (mDexterity / (float)target->getAgility()) * 100;

	if (enemyHitRate > 80)
	{
		enemyHitRate = 80;
	}
	else if (enemyHitRate < 20)
	{
		enemyHitRate = 20;
	}

	cout << mName << " is preparing to attack..." << endl;
	cout << endl;
	system("pause");

	setAttackDamage(enemyDamage);

	if (enemyHitRate > rand() % 100)
	{
		target->setHp(target->getHp() - enemyDamage);
		cout << mName << " attacked " << target->getName() << " and caused " << enemyDamage << " damage!" << endl;
	}
	else
	{
		cout << mName << " missed!" << endl;
	}

	cout << endl;
	system("pause");
}

void Enemy::setName(string name)
{
	mName = name;
}

void Enemy::setClass(string className)
{
	mClass = className;
}

void Enemy::setHp(int value)
{
	mHp = value;
	if (mHp < 0) mHp = 0;
}

void Enemy::setPower(int value)
{
	mPower = value;
}

void Enemy::setVitality(int value)
{
	mVitality = value;
}

void Enemy::setAgility(int value)
{
	mAgility = value;
}

void Enemy::setDexterity(int value)
{
	mDexterity = value;
}

void Enemy::setAttackDamage(int value)
{
	mAttackDamage = value;
}

string Enemy::getName()
{
	return mName;
}

string Enemy::getClass()
{
	return mClass;
}

int Enemy::getHp()
{
	return mHp;
	if (mHp < 0) mHp = 0;
}

int Enemy::getPower()
{
	return mPower;
}

int Enemy::getVitality()
{
	return mVitality;
}

int Enemy::getAgility()
{
	return mAgility;
}

int Enemy::getDexterity()
{
	return mDexterity;
}

int Enemy::getAttackDamage()
{
	return mAttackDamage;
}
