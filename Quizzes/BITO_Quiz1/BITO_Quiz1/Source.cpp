#include <iostream>
#include <string>
#include <time.h>
#include <vector>

using namespace std;

void mmWager(int round, int& moneyEarned, int& mmLeft, int& wager)
{
	cout << "=====  E M P E R O R  C A R D  =====" << endl;
	cout << " " << endl;

	cout << "Millimeters left: " << mmLeft << endl;
	cout << "Money: " << moneyEarned << endl;
	cout << "Round: " << round << endl;

	cout << " " << endl;
	cout << "------------------------------------------------------ " << endl;

	cout << "Tonegawa: How many millimeters do you want to wager?" << endl; //Dialogue
	cout << " " << endl;

	cout << "How much will Kaiji wager? Pleaser enter valid amount." << endl;
	cin >> wager;// Player inputs amount 

	if (wager > mmLeft) // If player inputs amount lower than what is left
	{
		cout << "Invalid amount. Player cannot bet more than the remaining millimeters." << endl;
		cin >> wager;
	}

	system("cls");
}

void createDeck(vector<string>& playerDeck, vector<string>& aiDeck,  int round) //create deck for player
{

	if (round % 2 == 1)
	{
		playerDeck.push_back("Emperor");
		aiDeck.push_back("Slave");
		for (int i = 1; i <= 4; i++)
		{
			playerDeck.push_back("Citizen");
			aiDeck.push_back("Citizen");
		}
	}
	else
	{
		playerDeck.push_back("Slave");
		aiDeck.push_back("Emperor");
		for (int i = 1; i <= 4; i++)
		{
			playerDeck.push_back("Citizen");
			aiDeck.push_back("Citizen");
		}
	}
}

void printDeck(vector<string>& playerDeck, int round, int& chosenCard) // print player's deck 
{
	if (round % 2 == 1)
	{
		for (int i = 0; i < playerDeck.size(); i++)
		{
			cout << "[" << i << "]" << playerDeck[i] << endl;
		}

		cin >> chosenCard;//Player chooses card from deck

		system("cls");
	}
	else
	{
		for (int i = 0; i < playerDeck.size(); i++)
		{
			cout << "[" << i << "]" << playerDeck[i] << endl;
		}

		cin >> chosenCard;//Player chooses card from deck

		system("cls");
	}
}

void aiChoose(vector<string>& aiDeck, int round, int& randomCard) // AI will choose random card to play
{
	randomCard = rand() % aiDeck.size();

	cout << "[Tonegawa] " << aiDeck[randomCard] << endl;
}

void playerChoose(vector<string>& playerDeck, vector<string>& aiDeck, int round, int& chosenCard, int& randomCard)
{	
	cout << "Which card will Kaiji play?" << endl; //Player chooses card to play
	cout << "------------------------------------------------------ " << endl;

	printDeck(playerDeck, round, chosenCard); //Calls playerPLay function to display cards

	cout << "[Kaiji] " << playerDeck[chosenCard] << " vs ";

	aiChoose(aiDeck, round, randomCard);//Ai reveals chosen card
	cout << "------------------------------------------------------ " << endl;

}

void removeCard(vector<string>& playerDeck, vector<string>& aiDeck, int round, int& chosenCard, int& randomCard) //removes card from deck
{
	int playerRemove = -1,
		aiRemove = -1;

	for (int i = 0; i < playerDeck.size(); i++)
	{
		if (i == chosenCard)
		{
			playerRemove = i;
		}
	}
	for (int i = 0; i < aiDeck.size(); i++)
	{
		if (i == chosenCard)
		{
			aiRemove = i;
		}
	}

	if (playerRemove != -1 && aiRemove != -1)
	{
		playerDeck.erase(playerDeck.begin() + playerRemove);
		aiDeck.erase(aiDeck.begin() + playerRemove);
	}
	
}

void evaluateCards(vector<string>& playerDeck, vector<string>& aiDeck, int round, int& moneyEarned, int& wager, int& mmLeft, int& chosenCard, int& randomCard)
{
	for (int i = 0; i <= 4; i++)
	{
		int addMoney = 100000 * wager;
			

		if (playerDeck[chosenCard] == "Emperor" && aiDeck[randomCard] == "Citizen")
		{
			cout << "Kaiji Wins!" << endl;

			moneyEarned += addMoney;

			cout << "You earned " << addMoney << " yen" << endl;

			break;
		}
		else if (playerDeck[chosenCard] == "Emperor" && aiDeck[randomCard] == "Slave")
		{
			cout << "Tonegawa Wins!" << endl;

			mmLeft -= wager;

			cout << "You lost " << wager << " mm" << endl;

			break;
		}
		else if (playerDeck[chosenCard] == "Citizen" && aiDeck[randomCard] == "Slave")
		{
			cout << "Kaiji Wins!" << endl;

			moneyEarned += addMoney;

			cout << "You earned " << addMoney << " yen" << endl;

			break;
		}
		else if (playerDeck[chosenCard] == "Citizen" && aiDeck[randomCard] == "Emperor")
		{
			cout << "Tonegawa Wins!" << endl;

			mmLeft -= wager;

			cout << "You lost " << wager << " mm" << endl;

			break;
		}
		else if (playerDeck[chosenCard] == "Slave" && aiDeck[randomCard] == "Emperor")
		{
			cout << "Kaiji Wins!" << endl;

			moneyEarned += addMoney * 5;

			cout << "You earned " << addMoney * 5 << " yen" << endl;

			break;
		}
		else if (playerDeck[chosenCard] == "Slave" && aiDeck[randomCard] == "Citizen")
		{
			cout << "Tonegawa Wins!" << endl;

			mmLeft -= wager;

			cout << "You lost " << wager << " mm" << endl;

			break;
		}
		else
		{
			cout << "It's a tie!" << endl;

			removeCard(playerDeck, aiDeck, round, chosenCard, randomCard);
			playerChoose(playerDeck, aiDeck, round, chosenCard, randomCard);

		}
	}
}

void emptyDeck(vector<string>& playerDeck, vector<string>& aiDeck) //Will empty the player's deck and ai's deck
{
	playerDeck.clear();
	aiDeck.clear();
}

void playRound(vector<string>& playerDeck, vector<string>& aiDeck, int round, int& moneyEarned, int wager, int& mmLeft, int& chosenCard, int& randomCard)
{
	mmWager(round, moneyEarned, mmLeft, wager);
	createDeck(playerDeck, aiDeck, round);//Generates deck for player and ai
	playerChoose(playerDeck, aiDeck, round, chosenCard, randomCard); //Player chooses card
	evaluateCards(playerDeck, aiDeck, round, moneyEarned, wager, mmLeft, chosenCard, randomCard);//Evaluates cards chosen by player and ai
	emptyDeck(playerDeck, aiDeck); //After every turn, this function will empty the deck

	system("pause");
	system("cls");
}

int main()
{
	srand(time(0));

	//Variables
	int round = 1,
		mmLeft = 30,
		moneyEarned = 0,
		chosenCard,
		wager = 0,
		randomCard = 0;
	//Vectors
	vector<string> playerDeck;
	vector<string> aiDeck;

	//Opening Dialogue
	cout << "=====  E M P E R O R  C A R D  =====" << endl;
	cout << " " << endl;
	cout << "Tonegawa: Now, let's begin." << endl;
	cout << " " << endl;

	system("pause");
	system("cls");

	while (round <= 12)
	{
		if (mmLeft == 0)
		{
			cout << "[Bad Ending] Kaiji has wagered a total of 30 Millimeters. He has now lost his sense of hearing and" << endl;
			cout << "he was also unable to get enough money to clear for his debt." << endl;

			system("pause");
			system("cls");

			break;
		}

		playRound(playerDeck, aiDeck, round, moneyEarned, wager, mmLeft, chosenCard, randomCard);
		
		round++;//Round ended

	}

	//Determine Ending
	if (mmLeft > 0 && moneyEarned >= 2000000)
	{
		cout << "[Best Ending] Kaiji has cleared his debt." << endl;

		system("pause");
		system("cls");
	}
	else if (mmLeft > 0 && moneyEarned < 2000000)
	{
		cout << "[Meh Ending] Kaiji was able to escape his ear being sacrified...however," << endl;
		cout << "he was unable to get enough money to clear for his debt." << endl;

		system("pause");
		system("cls");

	}
	else if (mmLeft == 0 && moneyEarned >= 2000000 || moneyEarned < 2000000)
	{
		cout << "[Bad Ending] Kaiji has wagered a total of 30 Millimeters. He has now lost his sense of hearing and" << endl;
		cout << "he was also unable to get enough money to clear for his debt." << endl;

		system("pause");
		system("cls");

	
	}
	
}
